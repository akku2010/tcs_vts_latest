import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../../providers/api-service/api-service';
import { NotifModalPage } from './notif-modal';
import { TimePickerModal } from './time-picker/time-picker';
// Add this import after the rest
// import { NativeAudio } from '@ionic-native/native-audio';

@IonicPage()
@Component({
  selector: 'page-notif-setting',
  templateUrl: 'notif-setting.html',
})
export class NotifSettingPage {
  islogin: any;
  dataOriginal: any;
  fData: any = {};
  notifType: any;
  isAddEmail: boolean = false;
  emailList: any;
  isAddPhone: boolean = false;
  phonelist: any;
  shownotifdiv: boolean = true;
  newArray: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private modalCtrl: ModalController,
    public platform: Platform,
    // private nativeAudio: NativeAudio
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.getCustDetails();
    // console.log("check here: ", this.notifArray);
    // this.sortArray();


    // The Native Audio plugin can only be called once the platform is ready
    // this.platform.ready().then(() => {
    //   console.log("platform ready");

    //   // This is used to unload the track. It's useful if you're experimenting with track locations
    //   this.nativeAudio.unload('trackID').then(function () {
    //     console.log("unloaded audio!");
    //   }, function (err) {
    //     console.log("couldn't unload audio... " + err);
    //   });

    //   // 'trackID' can be anything
    //   this.nativeAudio.preloadComplex('trackID', 'assets/audio/test.mp3', 1, 1, 0).then(function () {
    //     console.log("audio loaded!");
    //   }, function (err) {
    //     console.log("audio failed: " + err);
    //   });
    // });

  }

  getCustDetails() {
    var _bUrl = this.apiCall.mainUrl + 'users/getCustumerDetail?uid=' + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        // debugger
        console.log("resp data: ", respData)
        if (respData.length === 0) {
          return;
        } else {
          if (respData.cust.alert) {
            var result = Object.keys(respData.cust.alert).map(function (key) {
              return [key, respData.cust.alert[key]];
            });
            this.newArray = result.map(function (r) {
              return {
                key: r[0],
                imgUrl: 'assets/notificationIcon/' + r[0] + '.png',
                keys: r[1]
              }
            });

            console.log("someArr: " + this.newArray)
          }
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter NotifSettingPage');
  }

  onChange(ev) {
    console.log("event: ", ev)
  }

  tab(key1, key2) {
    if (key1 && key2) {
      if (key1.keys[key2] === false) {
        key1.keys[key2] = false;
      } else {
        key1.keys[key2] = true;
      }
    }
    for (var t = 0; t < this.newArray.length; t++) {
      if (this.newArray[t].key === key1.key) {
        this.newArray[t].keys = key1.keys;
      }
    }
    var temp = [];
    for (var e = 0; e < this.newArray.length; e++) {
      temp.push({
        [this.newArray[e].key]: this.newArray[e].keys,
        key: this.newArray[e].key
      });
    }
    /* covert an array into object */
    var res = {};
    temp.forEach(function (a) { res[a.key] = a[a.key] })


    this.fData.contactid = this.islogin._id;
    this.fData.alert = res;
    var url = this.apiCall.mainUrl + 'users/editUserDetails';
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, this.fData)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("check stat: ", respData);
        if (respData) {
          this.toastCtrl.create({
            message: 'Setting Updated',
            duration: 1500,
            position: 'bottom'
          }).present();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  // tab1(key1, key2, value) {

  //   console.log("playing audio");

  //   // this.nativeAudio.play('trackID').then(function () {
  //   //   console.log("playing audio!");
  //   // }, function (err) {
  //   //   console.log("error playing audio: " + err);
  //   // });

  //   if (key1 && key2)
  //     if (!value) {
  //       if (this.notifArray[key1][key2] === 'false') {
  //         this.notifArray[key1][key2] = true;
  //       } else {
  //         this.notifArray[key1][key2] = false;
  //       }
  //     }
  //     // this.notifArray[key1][key2] = !!!this.notifArray[key1][key2];
  //     else {
  //       this.notifArray[key1][key2] = value;
  //     }
  //   // this.notifArray[key1][key2] = value;
  //   if (JSON.stringify(this.notifArray) === JSON.stringify(this.dataOriginal))
  //     return;

  //   this.fData.contactid = this.islogin._id;
  //   this.fData.alert = this.notifArray;
  //   var url = this.apiCall.mainUrl + 'users/editUserDetails';
  //   this.apiCall.startLoading().present();
  //   this.apiCall.urlpasseswithdata(url, this.fData)
  //     .subscribe(respData => {
  //       this.apiCall.stopLoading();
  //       console.log("check stat: ", respData);
  //       if (respData) {
  //         this.toastCtrl.create({
  //           message: 'Setting Updated',
  //           duration: 1500,
  //           position: 'bottom'
  //         }).present();
  //       }
  //     },
  //       err => {
  //         this.apiCall.stopLoading();
  //       });
  // }

  addEmail(noti) {
    // use AudioProvider to control selected track 
  
    this.notifType = noti.key;
    this.isAddEmail = true;
    var data = {
      "buttonClick": 'email',
      "notifType": this.notifType,
      "compData": {
        [this.notifType]: noti.keys
      }
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }

  addPhone(noti) {
    this.notifType = noti.key;
    this.isAddPhone = true;
    var data = {
      buttonClick: 'phone',
      "notifType": this.notifType,
      "compData": {[this.notifType]: noti.keys}
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }

  // openTimePicker(data) {
  //   const modal = this.modalCtrl.create(TimePickerModal, {
  //     data: data
  //   });
  //   modal.present();
  // }

}

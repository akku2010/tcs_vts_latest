import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
// import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-expenses',
  templateUrl: 'expenses.html',
})
export class ExpensesPage implements OnInit {
  portstemp: any[] = [];
  islogin: any;
  _vehId: any = {};
  selectedVehicle: any;
  datetimeStart: any;
  datetimeEnd: any;
  expensesData: any[] = [];
  total: number = 0;
  showBtn: boolean;

  constructor(
    private navCtrl: NavController,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    // private alertCtrl: AlertController,
    // private translate: TranslateService
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();
  }

  ngOnInit() {
    this.getdevices();

  }

  onTypeDetail(exp) {
    console.log(exp)
    this.navCtrl.push('ExpenseTypeDetailPage', {
      expense: exp,
      dateFrom: this.datetimeStart,
      dateTo: this.datetimeEnd,
      userId: this.islogin._id,
      portstemp: this.portstemp,
      vehId: this._vehId._id
    })
  }

  getExpenceTypes() {
    let _bUrl;
    this.total = 0;
    if (this._vehId._id != undefined) {
      _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString() + "&vehicle=" + this._vehId._id;
    } else {
      _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString();
    }
    // const _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString();
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(data => {
        this.apiCall.stopLoading()
        this.expensesData = [];
        this.expensesData = data.expenseobj;
        for (var j = 0; j < data.expenseobj.length; j++) {
          this.total += data.expenseobj[j].total;
        }
        console.log("expense type=> " + data);
      },
        err => {
          this.apiCall.stopLoading()
          console.log(err)
        })
  }

  addExpence() {
    this.navCtrl.push('AddExpensePage', { vehicleList: this.portstemp })
  }

  getExpenseList() {
    this.getExpenceTypes();
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter ExpensesPage');

  }
  ionViewWillEnter() {
    this.showBtn = false;
    this._vehId = {};
    this.selectedVehicle = undefined;
    console.log("view will enter");
    this.getExpenseList();
  }

  toastMessage(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  getid(veh) {
    this._vehId = veh;
    this.showBtn = true;
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    // this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          // this.apiCall.stopLoading();
          console.log(err);
        });
  }

}

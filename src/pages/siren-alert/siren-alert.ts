import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio';
@IonicPage()
@Component({
  selector: 'page-siren-alert',
  templateUrl: 'siren-alert.html',
})
export class SirenAlertPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private nativeAudio: NativeAudio
  ) {
    this.nativeAudio.preloadComplex('parkingViolation', 'assets/sound/notif.mp3', 1, 1, 0);
    if (navParams.get('vehName') !== null) {
      console.log("vehicle name is: ", navParams.get('vehName'))
    }
  }

  ionViewDidEnter() {
    debugger
    console.log('ionViewDidEnter SirenAlertPage');
    // can optionally pass a callback to be called when the file is done playing
    // this.nativeAudio.play('parkingViolation', () => {
    //   this.dismiss();
    //   console.log('uniqueId1 is done playing')
    // });

    this.nativeAudio.loop('parkingViolation');
  }

  dismiss() {
    this.navCtrl.pop();
    this.nativeAudio.stop('parkingViolation');
  }

}
